#include <algorithm>
#include <charconv>
#include <iostream>
#include <string.h>

#include "nonius/nonius.h++"

#define FMT_HEADER_ONLY
#include <fmt/format.h>

#define BENCHMARK
#define INLINE inline

static constexpr char FixSeparator = 1;
static constexpr char Fix42Prefix[] = "8=FIX.4.2\0019=xxx";
static constexpr char FixChecksum[] = "\00110=";

static constexpr char lookupTwo[] = "00010203040506070809101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263646566676869707172737475767778798081828384858687888990919293949596979899";

namespace detail
{
    template<unsigned rem, unsigned... digits>
    struct explode : explode<rem / 10, rem % 10, digits...> {};

    template<unsigned... digits>
    struct explode<0, digits...> {
        static constexpr const char TagStr[] = {FixSeparator, ('0' + digits)..., '='};
        static constexpr int TagStrLen() { return sizeof(TagStr); }
    };
}

struct UTCTimestamp{};
struct UTCTimestamp2{};

struct Precision2 {
    double mValue;
};

INLINE void WriteValue(char*& dst, int value) {
    std::to_chars_result result = std::to_chars(dst, dst + 20, value);
    dst = result.ptr;
}
INLINE void WriteValue(char*& dst, const double& value) {
    dst = fmt::format_to(dst, "{}", value);
}
INLINE void WriteTwoDigits(char* dst, int number) {
    memcpy(dst, lookupTwo + 2 * number, 2);
    dst += 2;
    //int first = number / 10;
    //dst[0] = '0' + first;
    //dst[1] = '0' + number - (first * 10);
}
// These can be constexpr calculated if we have precision as an int template param.
constexpr const double tick = 0.01;
constexpr const double halfTick = tick / 2.0;
constexpr const double mult = 1.0 / tick;
INLINE void WriteValue(char*& dst, const Precision2& value) {
    int before = (int)(value.mValue + halfTick);
    int after = int((value.mValue + halfTick) * 100.0) - 100 * before;
    WriteValue(dst, before);
    dst[0] = '.';
    ++dst;
    WriteTwoDigits(dst, after);
    dst += 2;
}
INLINE void WriteValue(char*& dst, char value) {
    *dst = value;
    ++dst;
}
INLINE void WriteValue(char*& dst, const std::string_view& value) {
    memcpy(dst, value.data(), value.length());
    dst += value.length();
}
INLINE void WriteValue(char*& dst, UTCTimestamp) {
    timespec tp;
    clock_gettime(CLOCK_REALTIME, &tp);
    time_t nowSecond = tp.tv_sec;
    std::tm* timeNow = std::gmtime(&nowSecond);
    fmt::format_to(dst, "{:04d}{:02d}{:02d}-{:02d}:{:02d}:{:02d}.{:03d}\n",
        timeNow->tm_year + 1900,
        timeNow->tm_mon + 1,
        timeNow->tm_mday,
        timeNow->tm_hour,
        timeNow->tm_min,
        timeNow->tm_sec,
        tp.tv_nsec / 1'000'000);
    dst += 21;
}
INLINE void WriteTimestamp(char* dst, std::tm* timeNow, timespec& tp) {
    std::to_chars(dst, dst + 20, timeNow->tm_year + 1900);
    WriteTwoDigits(dst + 4, timeNow->tm_mon + 1);
    WriteTwoDigits(dst + 6, timeNow->tm_mday);
    dst[8] = '-';
    WriteTwoDigits(dst + 9, timeNow->tm_hour);
    dst[11] = ':';
    WriteTwoDigits(dst + 12, timeNow->tm_min);
    dst[14] = ':';
    WriteTwoDigits(dst + 15, timeNow->tm_sec);
    dst[17] = '.';

    int count = tp.tv_nsec / 1'000'000;
    int first = count / 100;
    dst[18] = '0' + first;
    count -= (first * 100);
    first = count / 10;
    dst[19] = '0' + first;
    count -= (first * 10);
    dst[20] = '0' + count;
}
INLINE void WriteValue(char*& dst, UTCTimestamp2) {
    timespec tp;
    clock_gettime(CLOCK_REALTIME, &tp);
    time_t nowSecond = tp.tv_sec;
    std::tm* timeNow = std::gmtime(&nowSecond);

    WriteTimestamp(dst, timeNow, tp);

    /*
    if (timeNow->tm_mon < 9) {
        dst[4] = '0';
        dst[5] = '1' + timeNow->tm_mon;
    }
    else {
        dst[4] = '1';
        dst[5] = '0' + timeNow->tm_mon - 9;
    }
    */
    //int dayFirst = timeNow->tm_mday / 10;
    //dst[6] = '0' + dayFirst;
    //dst[7] = '0' + timeNow->tm_mday - (10 * dayFirst);
    //dst[8] = '-';
    //dst[

    dst += 21;
}


template<unsigned Tag, typename TValue>
struct Fix {
    static INLINE constexpr const char* TagStr = detail::explode<Tag>::TagStr;
    static INLINE constexpr const int TagStrLen = detail::explode<Tag>::TagStrLen();

    const TValue& mValue;
    Fix(const TValue& value) : mValue(value){
    }

    INLINE void Serialize(char*& dst) const {
        memcpy(dst, TagStr, TagStrLen);
        dst += TagStrLen;
        WriteValue(dst, mValue);
    }
};


template<unsigned Tag, typename TValue>
auto FixField(const TValue& value) {
    return Fix<Tag, TValue>(value);
}

template<typename... Fields>
INLINE void WriteFix(char*& dst, const Fields&... fields) {
    char* start = dst;
    memcpy(dst, Fix42Prefix, sizeof(Fix42Prefix));
    dst += sizeof(Fix42Prefix);
    ((fields.Serialize(dst)), ...);
    int size = (dst - start) - 16;
    fmt::format_to(start + 12, "{:03d}", size);
    uint8_t checksum{1}; // Closing delimiter, which isn't in the string yet.
    for (char *ptr = start; ptr != dst; ++ptr)
        checksum += *ptr;
    memcpy(dst, FixChecksum, sizeof(FixChecksum));
    dst += sizeof(FixChecksum);
    fmt::format_to(dst, "{:03d}\001", checksum);
    dst += 4;
}

void Output(const char* buffer, size_t length) {
    std::string str(buffer, length);
    std::replace(str.begin(), str.end(), FixSeparator, '|');
    std::cout << str << "\n";
}



char buffer[2048];
char* ptr = buffer;
#ifdef BENCHMARK
int main() {
    nonius::configuration cfg;
    nonius::benchmark_registry benchmarks = {
        nonius::benchmark("FixToString_Baseline", [] {
                 ptr = buffer;
                 WriteFix(ptr,
                          FixField<35>("AE"),
                          FixField<49>("LSEHub"),
                          FixField<56>("LSETR"),
                          FixField<115>("BROKERX"),
                          FixField<34>(2287),
                          FixField<43>('N'),
                          FixField<571>("00008661533TRLO1-1-1-0"),
                          FixField<150>('H'));
                  return ptr;
            }),
        nonius::benchmark("FixToString_AddChar", [] {
                 ptr = buffer;
                 WriteFix(ptr,
                          FixField<35>("AE"),
                          FixField<49>("LSEHub"),
                          FixField<56>("LSETR"),
                          FixField<115>("BROKERX"),
                          FixField<34>(2287),
                          FixField<32>('X'),
                          FixField<43>('N'),
                          FixField<571>("00008661533TRLO1-1-1-0"),
                          FixField<150>('H'));
                  return ptr;
            }),
        nonius::benchmark("FixToString_Add16CharString", [] {
                 ptr = buffer;
                 WriteFix(ptr,
                          FixField<35>("AE"),
                          FixField<49>("LSEHub"),
                          FixField<56>("LSETR"),
                          FixField<115>("BROKERX"),
                          FixField<34>(2287),
                          FixField<32>("0123456789abcdef"),
                          FixField<43>('N'),
                          FixField<571>("00008661533TRLO1-1-1-0"),
                          FixField<150>('H'));
                  return ptr;
            }),
        nonius::benchmark("FixToString_AddInteger", [] {
                 ptr = buffer;
                 WriteFix(ptr,
                          FixField<35>("AE"),
                          FixField<49>("LSEHub"),
                          FixField<56>("LSETR"),
                          FixField<115>("BROKERX"),
                          FixField<34>(2287),
                          FixField<32>(16161),
                          FixField<43>('N'),
                          FixField<571>("00008661533TRLO1-1-1-0"),
                          FixField<150>('H'));
                  return ptr;
            }),
        nonius::benchmark("FixToString_AddTimestamp", [] {
                 ptr = buffer;
                 WriteFix(ptr,
                          FixField<35>("AE"),
                          FixField<49>("LSEHub"),
                          FixField<56>("LSETR"),
                          FixField<115>("BROKERX"),
                          FixField<34>(2287),
                          FixField<43>('N'),
                          FixField<52>(UTCTimestamp()),
                          FixField<571>("00008661533TRLO1-1-1-0"),
                          FixField<150>('H'));
                  return ptr;
            }),
        nonius::benchmark("FixToString_AddTimestamp2", [] {
                 ptr = buffer;
                 WriteFix(ptr,
                          FixField<35>("AE"),
                          FixField<49>("LSEHub"),
                          FixField<56>("LSETR"),
                          FixField<115>("BROKERX"),
                          FixField<34>(2287),
                          FixField<43>('N'),
                          FixField<52>(UTCTimestamp2()),
                          FixField<571>("00008661533TRLO1-1-1-0"),
                          FixField<150>('H'));
                  return ptr;
            }),
        nonius::benchmark("FixToString_AddFloat", [] {
                 ptr = buffer;
                 WriteFix(ptr,
                          FixField<35>("AE"),
                          FixField<49>("LSEHub"),
                          FixField<56>("LSETR"),
                          FixField<115>("BROKERX"),
                          FixField<34>(2287),
                          FixField<43>('N'),
                          FixField<44>(12.34),
                          FixField<571>("00008661533TRLO1-1-1-0"),
                          FixField<150>('H'));
                  return ptr;
            }),
        nonius::benchmark("FixToString_AddFloat2", [] {
                 ptr = buffer;
                 WriteFix(ptr,
                          FixField<35>("AE"),
                          FixField<49>("LSEHub"),
                          FixField<56>("LSETR"),
                          FixField<115>("BROKERX"),
                          FixField<34>(2287),
                          FixField<43>('N'),
                          FixField<44>(Precision2{12.34}),
                          FixField<571>("00008661533TRLO1-1-1-0"),
                          FixField<150>('H'));
                  return ptr;
            })};

    nonius::go(cfg, benchmarks);
}
#endif
#ifndef BENCHMARK

int main() {
    std::cout << "start\n";

    char buffer[2048];
    char* ptr = buffer;
    
    for (int i = 0; i < 1000000; ++i) {
        ptr = buffer;
        WriteFix(ptr,
                 FixField<35>('A'),
                 FixField<49>("SERVER"),
                 FixField<56>("CLIENT"),
                 FixField<34>(177),
                 FixField<52>(UTCTimestamp()),
                 FixField<52>(UTCTimestamp2()),
                 FixField<98>(0),
                 FixField<44>(12.34),
                 FixField<44>(Precision2{12.34}),
                 FixField<108>(30));
    }
    Output(buffer, ptr - buffer);
    std::string s;
    s.resize(22, 0);
    std::tm tm;
    tm.tm_year = 121;
    tm.tm_mon = 1;
    tm.tm_mday = 24;
    tm.tm_hour = 5;
    tm.tm_min = 34;
    tm.tm_sec = 4;
    timespec tp;
    tp.tv_nsec = 999999999;
    WriteTimestamp(&s[0], &tm, tp);
    std::cout << "'" << s << "'\n";

//    8=FIX.4.2|9=130|35=AE|49=LSEHub|56=LSETR|115=BROKERX|34=2287|43=N|52=20120330-12:14:09|370=20120330-12:14:09.816|571=00008661533TRLO1-1-1-0|150=H|10=074|
//    8=FIX.4.2|9=xxx|35=AE|49=LSEHub|56=LSETR|115=BROKERX|34=2287|43=N|52=20120330-12:14:09|370=20120330-12:14:09.816|571=00008661533TRLO1-1-1-0|150=H|10=
//    8=FIX.4.2|9=65|35=A|49=SERVER|56=CLIENT|34=177|52=20090107-18:15:16|98=0|108=30|10=062|

    std::cout << "end\n";
}
#endif
